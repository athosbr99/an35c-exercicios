package org.athoscastro.msg;

public class SMS implements Mensagem {
    @Override
    public void para(String numero) {
        System.out.println("Enviando mensagem para: " + numero);
    }

    @Override
    public void mensagem(String texto) {
        System.out.println("Corpo da mensagem: " + texto);
    }

    @Override
    public void enviar() {
        System.out.println("Mensagem enviada.");
    }
}
