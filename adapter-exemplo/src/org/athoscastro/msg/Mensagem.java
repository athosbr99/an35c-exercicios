package org.athoscastro.msg;

public interface Mensagem {
    void para(String numero);
    void mensagem(String texto);
    void enviar();
}
