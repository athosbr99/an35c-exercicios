package org.athoscastro.msg;

public class WhatsApp implements MessagePhone {
    @Override
    public void message(String contact, String text) {
        System.out.println("To: " + contact);
        System.out.println("Message: " + text);
    }

    @Override
    public void enviar() {
        System.out.println("Message sent.");
    }
}
