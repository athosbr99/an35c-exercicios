package org.athoscastro.msg;

public class WhatsAppAdapter implements Mensagem {

    public WhatsApp wa;
    public String numero;

    public WhatsAppAdapter(WhatsApp wa) {
        this.wa = wa;
    }

    @Override
    public void para(String numero) {
        this.numero = numero;
    }

    @Override
    public void mensagem(String texto) {
        this.wa.message(this.numero, texto);
    }

    @Override
    public void enviar() {
        this.wa.enviar();
    }
}
