package org.athoscastro.app;

import org.athoscastro.msg.Mensagem;
import org.athoscastro.msg.WhatsApp;
import org.athoscastro.msg.WhatsAppAdapter;
import org.athoscastro.tomada.PadraoBrasileiro;
import org.athoscastro.tomada.AdapterPadraoBrasileiro;

public class Main {

    public static void main(String[] args) {
        PadraoBrasileiro tomadabr = new PadraoBrasileiro();
        AdapterPadraoBrasileiro adaptador = new AdapterPadraoBrasileiro(tomadabr);
        adaptador.ligarTomadaTresPinos();
        Mensagem msg = new WhatsAppAdapter(new WhatsApp());
        msg.para("4898875752");
        msg.mensagem("Ayyyyyy");
        msg.enviar();
    }
}
