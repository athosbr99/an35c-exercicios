package org.athoscastro.tomada;

public class AdapterPadraoBrasileiro extends PadraoAmericano {

    private PadraoBrasileiro tomadaBrasileira;

    public AdapterPadraoBrasileiro(PadraoBrasileiro tomadaBrasileira) {
        this.tomadaBrasileira = tomadaBrasileira;
    }

    public boolean ligarTomadaTresPinos() {
        return this.tomadaBrasileira.ligarTomadaBrasileira();
    }

}
