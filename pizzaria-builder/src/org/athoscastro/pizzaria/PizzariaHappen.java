package org.athoscastro.pizzaria;

import org.athoscastro.builder.Pizzaria;
import org.athoscastro.builder.Pizza;
import org.athoscastro.pizza.PizzaHappenBanana;
import org.athoscastro.pizza.PizzaHappenFrango;
import org.athoscastro.pizza.PizzaHappenQueijo;

public class PizzariaHappen extends Pizzaria {
    public Pizza criarPizza(String sabor) {
        switch (sabor) {
            case "queijo":
                return new PizzaHappenQueijo();
            case "frango":
                return new PizzaHappenFrango();
            case "banana":
                return new PizzaHappenBanana();
            default:
                throw new NullPointerException();
        }
    }
}
