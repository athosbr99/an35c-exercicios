package org.athoscastro.pizzaria;

import org.athoscastro.builder.Pizza;
import org.athoscastro.builder.Pizzaria;
import org.athoscastro.pizza.PizzaBaggioMarguerita;
import org.athoscastro.pizza.PizzaBaggioQueijo;
import org.athoscastro.pizza.PizzaBaggioStrogonoff;

public class PizzariaBaggio extends Pizzaria {

    public final String pizzaria = "Baggio";

    @Override
    public Pizza criarPizza(String sabor) {
        switch (sabor) {
            case "queijo":
                return new PizzaBaggioQueijo();
            case "marguerita":
                return new PizzaBaggioMarguerita();
            case "strogonoff":
                return new PizzaBaggioStrogonoff();
            default:
                throw new NullPointerException();
        }
    }
}
