package org.athoscastro.pizza;

import org.athoscastro.builder.Pizza;

public class PizzaHappenQueijo extends Pizza {
    @Override
    public String getNome() {
        return "Queijo";
    }
}
