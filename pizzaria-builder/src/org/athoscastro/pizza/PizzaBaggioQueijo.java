package org.athoscastro.pizza;

import org.athoscastro.builder.Pizza;

public class PizzaBaggioQueijo extends Pizza {

    @Override
    public String getNome() {
        return "Queijo";
    }
}
