package org.athoscastro.pizza;

import org.athoscastro.builder.Pizza;

public class PizzaBaggioStrogonoff extends Pizza {

    @Override
    public String getNome() {
        return "Strogonoff";
    }
}
