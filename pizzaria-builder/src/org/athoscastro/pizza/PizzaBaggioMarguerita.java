package org.athoscastro.pizza;

import org.athoscastro.builder.Pizza;

public class PizzaBaggioMarguerita extends Pizza {

    @Override
    public String getNome() {
        return "Marguerita";
    }
}
