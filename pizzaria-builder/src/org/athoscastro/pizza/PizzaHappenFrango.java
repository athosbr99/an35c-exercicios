package org.athoscastro.pizza;

import org.athoscastro.builder.Pizza;

public class PizzaHappenFrango extends Pizza {
    @Override
    public String getNome() {
        return "Frango";
    }
}
