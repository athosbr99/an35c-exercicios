package org.athoscastro.pizza;

import org.athoscastro.builder.Pizza;

public class PizzaHappenBanana extends Pizza {
    @Override
    public String getNome() {
        return "Banana";
    }
}
