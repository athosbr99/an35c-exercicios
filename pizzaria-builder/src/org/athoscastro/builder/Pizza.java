package org.athoscastro.builder;

public abstract class Pizza {
    protected String nome;
    private Double valor;
    private String opicional;

    public abstract String getNome();


    public void cobrar() {
        System.out.println("Cobrando...");
    }

    public void preparar() {
        System.out.println("Preparando pizza de " + getNome() + "...");
    }

    public void solicitarEntrega() {
        System.out.println("Solicitando entrega...");
        System.out.println("Entregue.");
    }
}
