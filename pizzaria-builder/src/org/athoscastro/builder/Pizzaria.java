package org.athoscastro.builder;

public abstract class Pizzaria {
    public Pizza pedirPizza(String sabor) {
        Pizza p = criarPizza(sabor);
        p.cobrar();
        p.preparar();
        p.solicitarEntrega();
        return p;
    }

    public abstract Pizza criarPizza(String sabor);

}
