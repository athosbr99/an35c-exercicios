package org.athoscastro.app;

import org.athoscastro.builder.Pizza;
import org.athoscastro.builder.Pizzaria;
import org.athoscastro.pizzaria.PizzariaHappen;

public class Main {

    public static void main(String[] args) {
        Pizzaria p = new PizzariaHappen();
        try {
            Pizza pizza = p.pedirPizza("queijo");
            System.out.println(pizza.getNome());

        } catch (NullPointerException ex) {
            System.out.println("Pizza não encontrada.");
        }
    }
}
