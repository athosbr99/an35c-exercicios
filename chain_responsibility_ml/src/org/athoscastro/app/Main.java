package org.athoscastro.app;

import org.athoscastro.divisaoCompra.Checkout;
import org.athoscastro.divisaoCompra.Compra;

public class Main {

    public static void main(String[] args) {
        Checkout c = new Checkout();
        Compra compra = new Compra(5000);
        c.calculaDesconto(compra);
    }
}
