package org.athoscastro.divisaoCompra;

public abstract class RegraDivisao {
    protected double valorTotalDesconto;
    public abstract double calcula(Compra compra);
    public abstract void setProximo(RegraDivisao proximo);
}
