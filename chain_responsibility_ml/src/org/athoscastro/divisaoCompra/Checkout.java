package org.athoscastro.divisaoCompra;

public class Checkout {

    public double calculaDesconto(Compra compra) {
        RegraDivisao r1 = new Vendedor();
        RegraDivisao r2 = new MercadoLivre();
        RegraDivisao r3 = new Impostos();
        r1.setProximo(r2);
        r2.setProximo(r3);
        return r1.calcula(compra);
    }
}
