package org.athoscastro.divisaoCompra;

public class Compra {

    private double valor;

    public Compra(double valor) {
        this.valor = valor;
    }

    public double getValor() {
        return this.valor;
    }

}
