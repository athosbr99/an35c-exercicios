package org.athoscastro.divisaoCompra;

public class MercadoLivre extends RegraDivisao {
    private RegraDivisao proximo;
    @Override
    public double calcula(Compra compra) {
        this.valorTotalDesconto += (compra.getValor()*10)/100;
        System.out.println("Valor retirado: " + this.valorTotalDesconto);
        return (proximo != null) ? this.valorTotalDesconto + proximo.calcula(compra) : this.valorTotalDesconto;
    }

    @Override
    public void setProximo(RegraDivisao proximo) {
        this.proximo = proximo;
    }
}
