package org.athoscastro.divisaoCompra;

public class Impostos extends RegraDivisao {
    private RegraDivisao proximo;
    @Override
    public double calcula(Compra compra) {
        this.valorTotalDesconto += (compra.getValor()*5)/100;
        System.out.println("Valor retirado: " + this.valorTotalDesconto);
        if (proximo == null) {
            return this.valorTotalDesconto;
        }
        return this.valorTotalDesconto + proximo.calcula(compra);
    }

    @Override
    public void setProximo(RegraDivisao proximo) {
        this.proximo = proximo;
    }
}
