package org.athoscastro.gateway;

import org.athoscastro.boleto.BoletoInterface;
import org.athoscastro.cartao.CartaoInterface;

public interface PagamentoFactory {
    BoletoInterface gerarBoleto();
    CartaoInterface cadastrarCartao();
}
