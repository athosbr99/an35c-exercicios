package org.athoscastro.gateway;

import org.athoscastro.boleto.BoletoInterface;
import org.athoscastro.boleto.BoletoBradesco;
import org.athoscastro.cartao.CartaoInterface;
import org.athoscastro.cartao.CartaoMasterCard;

public class PagSeguroFactory implements PagamentoFactory {
    public BoletoInterface gerarBoleto() {
        return new BoletoBradesco();
    }

    public CartaoInterface cadastrarCartao() {
        return new CartaoMasterCard();
    }
}
