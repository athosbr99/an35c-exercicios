package org.athoscastro.gateway;

import org.athoscastro.boleto.BoletoInterface;
import org.athoscastro.boleto.BoletoItau;
import org.athoscastro.cartao.CartaoInterface;
import org.athoscastro.cartao.CartaoVisa;

public class MoipFactory implements PagamentoFactory {
    public BoletoInterface gerarBoleto() {
        return new BoletoItau();
    }

    public CartaoInterface cadastrarCartao() {
        return new CartaoVisa();
    }
}
