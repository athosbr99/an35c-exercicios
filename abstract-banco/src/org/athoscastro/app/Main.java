package org.athoscastro.app;

import org.athoscastro.gateway.MoipFactory;
import org.athoscastro.gateway.PagSeguroFactory;
import org.athoscastro.gateway.PagamentoFactory;

public class Main {
    public static void main(String[] args) {
        PagamentoFactory pagamento = new PagSeguroFactory();
        System.out.println(pagamento.gerarBoleto().dadosBoleto());
        System.out.println(pagamento.cadastrarCartao().identificarCartao());
        pagamento = new MoipFactory();
        System.out.println(pagamento.gerarBoleto().dadosBoleto());
        System.out.println(pagamento.cadastrarCartao().identificarCartao());
    }
}
