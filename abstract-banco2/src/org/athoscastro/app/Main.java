package org.athoscastro.app;

import org.athoscastro.banco.*;

public class Main {

    public static void main(String[] args) {
        BancoFactory bb = new BancoBB();
        BancoFactory itau = new BancoItau();
        System.out.println(bb.identificarBanco());
        System.out.println(bb.cadastrarCartaoCredito().identificarCartao());
        System.out.println(bb.cadastrarCartaoDebito().identificarCartao());
        System.out.println(itau.identificarBanco());
        System.out.println(itau.cadastrarCartaoCredito().identificarCartao());
        System.out.println(itau.cadastrarCartaoDebito().identificarCartao());
    }
}
