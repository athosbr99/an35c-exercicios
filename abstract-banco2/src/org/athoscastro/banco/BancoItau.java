package org.athoscastro.banco;

import org.athoscastro.cartoes.CartaoFactory;
import org.athoscastro.cartoes.CartaoMasterCardCredito;
import org.athoscastro.cartoes.CartaoMasterCardDebito;

public class BancoItau implements BancoFactory {

    @Override
    public CartaoFactory cadastrarCartaoDebito() {
        return new CartaoMasterCardDebito();
    }

    @Override
    public CartaoFactory cadastrarCartaoCredito() {
        return new CartaoMasterCardCredito();
    }

    @Override
    public String identificarBanco() {
        return "Você está no Itau.";
    }
}
