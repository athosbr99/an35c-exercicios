package org.athoscastro.banco;

import org.athoscastro.cartoes.CartaoFactory;

public interface BancoFactory {
    CartaoFactory cadastrarCartaoCredito();
    CartaoFactory cadastrarCartaoDebito();
    String identificarBanco();
}
