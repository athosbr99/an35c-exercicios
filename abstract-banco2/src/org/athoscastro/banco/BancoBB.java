package org.athoscastro.banco;

import org.athoscastro.cartoes.CartaoFactory;
import org.athoscastro.cartoes.CartaoVisaDebito;
import org.athoscastro.cartoes.CartaoVisaCredito;

public class BancoBB implements BancoFactory {
    public String identificarBanco() {
        return "Você está no Banco do Brasil.";
    }

    @Override
    public CartaoFactory cadastrarCartaoCredito() {
        return new CartaoVisaCredito();
    }

    @Override
    public CartaoFactory cadastrarCartaoDebito() {
        return new CartaoVisaDebito();
    }
}
