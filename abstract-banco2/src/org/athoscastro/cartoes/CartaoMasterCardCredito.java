package org.athoscastro.cartoes;

public class CartaoMasterCardCredito implements CartaoFactory {
    @Override
    public String identificarCartao() {
        return "Este cartão de crédito é da MasterCard.";
    }
}
