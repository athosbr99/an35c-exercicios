package org.athoscastro.cartoes;

public interface CartaoFactory {
    String identificarCartao();
}
