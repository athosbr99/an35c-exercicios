package org.athoscastro.cartoes;

public class CartaoVisaCredito implements CartaoFactory {
    @Override
    public String identificarCartao() {
        return "Este cartão de crédito é da Visa.";
    }
}
