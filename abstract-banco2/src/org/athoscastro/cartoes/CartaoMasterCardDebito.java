package org.athoscastro.cartoes;

public class CartaoMasterCardDebito implements CartaoFactory {
    public String identificarCartao() {
        return "Este cartão de débito é da MasterCard.";
    }
}
