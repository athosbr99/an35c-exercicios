package org.athoscastro.cartoes;

public class CartaoVisaDebito implements CartaoFactory {
    public String identificarCartao() {
        return "Este cartão de débito é da Visa.";
    }
}
