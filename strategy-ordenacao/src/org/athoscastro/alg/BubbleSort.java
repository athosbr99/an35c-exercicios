package org.athoscastro.alg;

public class BubbleSort implements Ordenar {
    public int[] ordenar(int[] vet) {
        int i;
        int aux;
        for (i = 0; i < vet.length; i++) {
            for (int j = 0; j < (vet.length - 1); j++) {
                if (vet[j] > vet[j + 1]) {
                    aux = vet[j];
                    vet[j] = vet[j + 1];
                    vet[j + 1] = aux;
                }
            }
        }
        return vet;
    }
}
