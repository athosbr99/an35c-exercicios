package org.athoscastro.app;

import org.athoscastro.alg.*;

public class Main {

    public static void main(String[] args) {
        int[] vetor = {1, 3, 4, 5, 240, 20, 2};
        Ordenador ord = new Ordenador();

        int[] bub = ord.ordena(new BubbleSort(), vetor);
        int[] quick = ord.ordena(new QuickSort(), vetor);
        int[] selec = ord.ordena(new SelectionSort(), vetor);

        System.out.println("Bubble Sort:");
        for (int aBub : bub) {
            System.out.println(aBub);
        }
        System.out.println("Quick sort:");
        for (int q : quick) {
            System.out.println(q);
        }
        System.out.println("Selection sort:");
        for (int s : selec) {
            System.out.println(s);
        }
    }
}
