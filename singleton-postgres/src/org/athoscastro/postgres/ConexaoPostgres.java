/*
Esse código utiliza o banco de dados PostgreSQL.

Crie um banco de dados e uma tabela com a seguinte query para utilizar:

create database alunos_an35c;
create table alunos (id serial primary key, nome varchar(30) not null);

Esse projeto foi criado com o IntelliJ IDEA, mas deve(ria) funcionar com o Eclipse.

Não esqueça de adicionar o JAR do JDBC do PostgreSQL, se necessário.
 */

package org.athoscastro.postgres;

import java.sql.*;
import java.util.ArrayList;

public class ConexaoPostgres {
    private static ConexaoPostgres instancia;
    private ConexaoPostgres() {

    }

    public static ConexaoPostgres getInstancia() {
        if (instancia == null) {
            instancia = new ConexaoPostgres();
        }
        return instancia;
    }

    public void setAluno(Alunos aluno) {
        String SQL = "INSERT INTO alunos(nome) VALUES (?)";
        try (Connection conn = conexao(); PreparedStatement pst = conn.prepareStatement(SQL)) {
            pst.setString(1, aluno.getNome());
            pst.executeUpdate();
        } catch (SQLException exep) {
            System.out.println(exep.getMessage());
        }
    }

    public Alunos getAluno(int id) {
       String SQL = "select * from alunos where id = ?";
       Alunos aluno = null;
       try (Connection conn = conexao(); PreparedStatement pst = conn.prepareStatement(SQL)) {
           pst.setInt(1, id);
           ResultSet rs = pst.executeQuery();
           while (rs.next()) {
               aluno = new Alunos(rs.getInt("id"), rs.getString("nome"));
           }
       } catch (SQLException exep) {
           System.out.println(exep.getMessage());
       }
       return aluno;
    }

    public ArrayList<Alunos> getTodosAlunos() {
        String SQL = "select * from alunos";
        ArrayList<Alunos> nomes = new ArrayList<>();
        try (Connection conn = conexao(); PreparedStatement pst = conn.prepareStatement(SQL)) {
            ResultSet rs = pst.executeQuery();
            while(rs.next()) {
                Alunos aluno = new Alunos(rs.getInt("id"), rs.getString("nome"));
                nomes.add(aluno);
            }
        } catch (SQLException exep) {
            System.out.println(exep.getMessage());
        }
        return nomes;
    }

    private Connection conexao() {
        String driver = "org.postgresql.Driver";
        String user = "postgres";
        String url = "jdbc:postgresql://localhost:8432/alunos_an35c";
        String senha = "";
        Connection con = null;
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(url, user, senha);
        } catch (ClassNotFoundException ex) {
            System.err.print(ex.getMessage());
        } catch (SQLException e) {
            System.err.print(e.getMessage());
        }
        return con;
    }
}
