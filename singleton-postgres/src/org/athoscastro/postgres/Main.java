/*
Esse código utiliza o banco de dados PostgreSQL.

Crie um banco de dados e uma tabela com a seguinte query para utilizar:

create database alunos_an35c;
create table alunos (id serial primary key, nome varchar(30) not null);

Esse projeto foi criado com o IntelliJ IDEA, mas deve(ria) funcionar com o Eclipse.

Não esqueça de adicionar o JAR do JDBC do PostgreSQL, se necessário.
 */

package org.athoscastro.postgres;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args){
        ConexaoPostgres conexao = ConexaoPostgres.getInstancia();
        Alunos rodrigo = new Alunos();
        rodrigo.setNome("Rodrigo Augusto dos Santos");
        conexao.setAluno(rodrigo);
        Alunos augusto = new Alunos();
        augusto.setNome("Augusto Silvério Castro");
        conexao.setAluno(augusto);
        ArrayList<Alunos> alunos = conexao.getTodosAlunos();
        for (Alunos x : alunos) {
            System.out.println(x.getId() + " - " + x.getNome());
        }
    }
}
