/*
Esse código utiliza o banco de dados PostgreSQL.

Crie um banco de dados e uma tabela com a seguinte query para utilizar:

create database alunos_an35c;
create table alunos (id serial primary key, nome varchar(30) not null);

Esse projeto foi criado com o IntelliJ IDEA, mas deve(ria) funcionar com o Eclipse.

Não esqueça de adicionar o JAR do JDBC do PostgreSQL, se necessário.
 */

package org.athoscastro.postgres;

public class Alunos {
    private int id;
    private String nome;

    public Alunos(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Alunos(){}

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
