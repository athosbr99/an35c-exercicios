package br.edu.utfpr.poo.funcionario;

import br.edu.utfpr.poo.regras.Cargo;
import br.edu.utfpr.poo.regras.Regras;

public class CalculaSalario {

    public double calcula(Funcionario funcionario) {
        Regras r = new Regras();
        Cargo c = funcionario.getCargo();
        switch (c) {
            case DESENVOLVEDOR:
                return r.dezVinte(funcionario);
            case TESTER:
            case DBA:
                return r.quinzeVinteCinco(funcionario);
            default:
                return 0;
        }
    }
}