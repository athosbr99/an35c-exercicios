package br.edu.utfpr.poo.regras;

import br.edu.utfpr.poo.funcionario.Funcionario;

@FunctionalInterface
public interface QuinzeVinteCinco {
    double quinzeVinteCinco(Funcionario funcionario);
}
