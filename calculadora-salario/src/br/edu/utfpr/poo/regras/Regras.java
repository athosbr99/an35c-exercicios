package br.edu.utfpr.poo.regras;

import br.edu.utfpr.poo.funcionario.Funcionario;

public class Regras implements DezVinte, QuinzeVinteCinco {

    @Override
    public double dezVinte(Funcionario funcionario) {
        if (funcionario.getSalarioBase() > 3000.0) {
            return funcionario.getSalarioBase() * 0.8;
        } else {
            return funcionario.getSalarioBase() * 0.9;
	}
    }

    @Override
    public double quinzeVinteCinco(Funcionario funcionario) {
        if (funcionario.getSalarioBase() > 2000.0) {
            return funcionario.getSalarioBase() * 0.75;
        } else {
            return funcionario.getSalarioBase() * 0.85;
        }
    }
}
