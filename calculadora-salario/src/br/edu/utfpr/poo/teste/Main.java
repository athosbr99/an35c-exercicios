package br.edu.utfpr.poo.teste;
import java.util.Calendar;
import java.util.GregorianCalendar;
import br.edu.utfpr.poo.funcionario.CalculaSalario;
import br.edu.utfpr.poo.regras.Cargo;
import br.edu.utfpr.poo.funcionario.Funcionario;

public class Main {
    public static void main(String[] args) {
	Funcionario desenvolvedor = new Funcionario();
	desenvolvedor.setCargo(Cargo.DESENVOLVEDOR);
	desenvolvedor.setDataDeAdmissao(new GregorianCalendar(2000, Calendar.JULY, 12));
	desenvolvedor.setId(1);
	desenvolvedor.setNome("joao");
	desenvolvedor.setSalarioBase(3001);
	CalculaSalario cs = new CalculaSalario();
	System.out.println(cs.calcula(desenvolvedor));
    }
}