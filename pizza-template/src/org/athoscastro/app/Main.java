package org.athoscastro.app;

import org.athoscastro.pizzas.Atum;
import org.athoscastro.pizzas.CincoQueijos;
import org.athoscastro.pizzas.Pizza;

public class Main {

    public static void main(String[] args) {
        Pizza cincoqueijo = new CincoQueijos();
        cincoqueijo.construir();
        System.out.println("--");
        Pizza atum = new Atum();
        atum.construir();
    }
}
