package org.athoscastro.pizzas;

import java.util.ArrayList;

public class CincoQueijos extends TemplatePizza {
    @Override
    public String molho() {
        return "Tomate";
    }

    @Override
    public String massa() {
        return "Pan";
    }

    @Override
    public ArrayList<String> recheio() {
        ArrayList<String> recheio = new ArrayList<>();
        recheio.add("Queijo muçarela");
        recheio.add("Queijo prato");
        recheio.add("Queijo provolone");
        recheio.add("Queijo gorgonzola");
        recheio.add("Catupiry");
        return recheio;
    }

    @Override
    public String adicional() {
        return "Bacon";
    }

    @Override
    public boolean assar() {
        return true;
    }

}
