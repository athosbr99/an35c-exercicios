package org.athoscastro.pizzas;

import java.util.ArrayList;

public abstract class TemplatePizza implements Pizza {
    public abstract String molho();

    public abstract String massa();

    public abstract ArrayList<String> recheio();

    public abstract String adicional();

    public abstract boolean assar();

    public void construir() {
        System.out.println("Seu molho: " + molho());
        System.out.println("Sua massa: " + massa());
        System.out.println("Seus recheios: ");
        for (String recheios : recheio()) {
            System.out.println(recheios);
        }
        System.out.println("Seu adicional: " + adicional());
        if (assar()) {
            System.out.println("Preparado pra assar.");
        } else {
            System.out.println("Pizza ainda não pronta para comércio.");
        }
    }
}
