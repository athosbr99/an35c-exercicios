package org.athoscastro.pizzas;

import java.util.ArrayList;

public class Atum extends TemplatePizza {
    @Override
    public String molho() {
        return "Tomate";
    }

    @Override
    public String massa() {
        return "Pan";
    }

    @Override
    public ArrayList<String> recheio() {
        ArrayList<String> recheio = new ArrayList<>();
        recheio.add("Atum");
        recheio.add("Oregano");
        recheio.add("Queijo muçarela");
        return recheio;
    }

    @Override
    public String adicional() {
        return "Milho";
    }

    @Override
    public boolean assar() {
        return true;
    }
}
