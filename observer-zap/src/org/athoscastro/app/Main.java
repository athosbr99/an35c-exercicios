package org.athoscastro.app;

import org.athoscastro.classes.Grupos;
import org.athoscastro.classes.Usuario;


public class Main {

    public static void main(String[] args) {
		Grupos grupo = new Grupos();
		Usuario jao = new Usuario(grupo, "jao");
		Usuario maria = new Usuario(grupo, "maria");
		grupo.attach(jao);
		grupo.attach(maria);

	}
}
