package org.athoscastro.classes;

import org.athoscastro.interfaces.Observer;

import java.util.List;

public class Usuario implements Observer {

    protected Grupos gr;
    private String nome;

    public Usuario(Grupos gr, String nome) {
        this.gr = gr;
        this.nome = nome;
    }

    @Override
    public void update() {
        System.out.println(nome);
    }
}
