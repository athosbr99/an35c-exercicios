package org.athoscastro.classes;

import org.athoscastro.interfaces.Observer;
import org.athoscastro.interfaces.Subject;

import java.util.ArrayList;
import java.util.List;

public class Grupos implements Subject {
    protected List<Observer> observers;

    public Grupos(){
        this.observers = new ArrayList<>();
    }

    @Override
    public void attach(Observer observer) {
        this.observers.add(observer);
        notificar();
    }

    @Override
    public void dettach(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notificar() {
        for(Observer observer: observers) {
            observer.update();
        }
    }
}
