package org.athoscastro.exerciciorest.model;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import com.google.gson.Gson;
import org.athoscastro.exerciciorest.model.Local;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Patrimonio {
	
	private Long id;
	private String nome;
	private Calendar data_adicao;
	private Local local;
	
	public Patrimonio() {
		
	}
	
	public Patrimonio(long id, String nome, Calendar data_adicao, Local local) {
		this.nome = nome;
		this.id = id;
		this.data_adicao = data_adicao;
		this.local = local;
	}
	
	
	
	public Local getLocal() {
		return local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Calendar getData_adicao() {
		return data_adicao;
	}
	public void setData_adicao(Calendar data_adicao) {
		this.data_adicao = data_adicao;
	}
	
	public String toJson() {
		return new Gson().toJson(this);
	}
}
