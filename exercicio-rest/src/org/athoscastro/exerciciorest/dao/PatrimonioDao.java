package org.athoscastro.exerciciorest.dao;

import java.util.HashMap;
import java.util.Map;
import org.athoscastro.exerciciorest.model.Patrimonio;
import java.util.Calendar;
import org.athoscastro.exerciciorest.model.Local;

public class PatrimonioDao {
	
	private static Map<Long, Patrimonio> salvo = new HashMap<>();
	
	static {
		salvo.put(1l, new Patrimonio(1l, "Cadeira", Calendar.getInstance(), new Local(1l, "Sala de Reuni�es - Predio Centro")));
		salvo.put(2l, new Patrimonio(2l, "Mesa", Calendar.getInstance(), new Local(2l, "Recep��o - Pr�dio Zona Norte")));
	}
	
	public Patrimonio buscar(Long id) {
		return salvo.get(id);
	}

}
