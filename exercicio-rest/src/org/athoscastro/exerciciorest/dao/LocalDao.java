package org.athoscastro.exerciciorest.dao;

import java.util.HashMap;
import java.util.Map;
import org.athoscastro.exerciciorest.model.Local;

public class LocalDao {
	private static Map<Long, Local> salvo = new HashMap<>();
	
	static {
		salvo.put(1l, new Local(1l, "Sala de Reuni�es - Predio Centro"));
		salvo.put(2l, new Local(2l, "Recep��o - Pr�dio Zona Norte"));
	}
	
	public Local buscar(Long id) {
		return salvo.get(id);
	}
}
