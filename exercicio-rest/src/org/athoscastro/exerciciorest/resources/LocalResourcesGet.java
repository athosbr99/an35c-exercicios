package org.athoscastro.exerciciorest.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.Produces;
import org.athoscastro.exerciciorest.dao.LocalDao;
import org.athoscastro.exerciciorest.model.Local;

@Path("local")
public class LocalResourcesGet {
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public String busca(@PathParam("id") long id) {
		LocalDao dao = new LocalDao();
		Local resultado = dao.buscar(id);
		return resultado.toJson();
	}
}
