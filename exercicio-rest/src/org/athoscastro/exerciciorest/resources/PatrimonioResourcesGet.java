package org.athoscastro.exerciciorest.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.Produces;
import org.athoscastro.exerciciorest.dao.PatrimonioDao;
import org.athoscastro.exerciciorest.model.Patrimonio;

@Path("patrimonio")
public class PatrimonioResourcesGet {
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public String busca(@PathParam("id") long id) {
		PatrimonioDao dao = new PatrimonioDao();
		Patrimonio resultado = dao.buscar(id);
		return resultado.toJson();
	}
}
