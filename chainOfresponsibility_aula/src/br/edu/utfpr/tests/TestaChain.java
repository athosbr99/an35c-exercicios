package br.edu.utfpr.tests;

import br.edu.utfpr.chain.CalculadoraDeDescontos;
import br.edu.utfpr.chain.Item;
import br.edu.utfpr.chain.Orcamento;

public class TestaChain {

	public static void main(String[] args) {

		CalculadoraDeDescontos calc = new CalculadoraDeDescontos();

		Orcamento orcamento = new Orcamento(true);

		orcamento.add(new Item(1000.0));
		orcamento.add(new Item(10.0));
		orcamento.add(new Item(10.0));
		orcamento.add(new Item(10.0));

		double desconto = calc.calculaDesconto(orcamento);

		System.out.println(desconto);
	}

}
