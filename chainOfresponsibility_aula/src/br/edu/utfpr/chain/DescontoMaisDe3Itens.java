package br.edu.utfpr.chain;

public class DescontoMaisDe3Itens extends RegraDesconto {

	private RegraDesconto proximo;

	@Override
	public double calcula(Orcamento orcamento) {
		// da um desconto
		// se cliente comprar mais de 3 itens
		if (orcamento.getItens().size() > 3) {
			this.valorTotalDesconto += orcamento.getValorOrcamento() * 0.05;
		}

		return (proximo != null) ? valorTotalDesconto + proximo.calcula(orcamento) : this.valorTotalDesconto;
	}

	@Override
	public void setProximo(RegraDesconto proximo) {
		this.proximo = proximo;

	}

}
