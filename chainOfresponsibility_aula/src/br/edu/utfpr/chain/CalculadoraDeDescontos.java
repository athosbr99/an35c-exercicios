package br.edu.utfpr.chain;

public class CalculadoraDeDescontos {

	public double calculaDesconto(Orcamento orcamento) {
		// regras de negocio
		RegraDesconto d1 = new DescontoMaisDe1000();
		RegraDesconto d2 = new DescontoMaisDe3Itens();
		RegraDesconto d3 = new DescontoSeTiverVoucher();
		
		// cadeia
		d1.setProximo(d2);
		d2.setProximo(d3);
		
		return d1.calcula(orcamento);
	}

}
