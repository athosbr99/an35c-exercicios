package br.edu.utfpr.chain;

public class DescontoMaisDe1000 extends RegraDesconto {

	private RegraDesconto proximo;

	@Override
	public double calcula(Orcamento orcamento) {
		// da uma desconto de 10% para compras
		// acima de 1000
		if (orcamento.getValorOrcamento() > 1000) {
			this.valorTotalDesconto += orcamento.getValorOrcamento() * 0.10;
		}

		return (proximo != null) ? this.valorTotalDesconto + proximo.calcula(orcamento) : this.valorTotalDesconto;
	}

	@Override
	public void setProximo(RegraDesconto proximo) {
		this.proximo = proximo;
	}

}
