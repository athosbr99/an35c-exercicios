package br.edu.utfpr.chain;

public abstract class RegraDesconto {
	
	protected double valorTotalDesconto;

	public abstract double calcula(Orcamento orcamento);

	public abstract void setProximo(RegraDesconto proximo);

}
