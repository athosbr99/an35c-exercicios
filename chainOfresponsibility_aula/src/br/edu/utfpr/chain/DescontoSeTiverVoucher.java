package br.edu.utfpr.chain;

public class DescontoSeTiverVoucher extends RegraDesconto {

	private RegraDesconto proximo;

	@Override
	public double calcula(Orcamento orcamento) {
		// se cliente tiver voucher
		// da desconto de 3 %
		if (orcamento.possuiVoucher()) {
			valorTotalDesconto += orcamento.getValorOrcamento() * 0.03;
		}

		return (proximo != null) ? valorTotalDesconto + proximo.calcula(orcamento) : valorTotalDesconto;

	}

	@Override
	public void setProximo(RegraDesconto proximo) {
		this.proximo = proximo;

	}

}
