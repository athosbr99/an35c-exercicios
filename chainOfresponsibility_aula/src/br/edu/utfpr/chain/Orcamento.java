package br.edu.utfpr.chain;

import java.util.ArrayList;
import java.util.List;

public class Orcamento {
	
	private List<Item> itens;
	private boolean voucher;
	
	public Orcamento(boolean voucher) {
		this.voucher = voucher;
		itens = new ArrayList<>();
	}
	
	
	public void add(Item item) {
		itens.add(item);
	}
	
	public double getValorOrcamento() {
		double valor = 0.0;
		
		for (Item item : itens) {
			valor += item.getValor();
		}
		
		return valor;
	}


	public List<Item> getItens() {
		return itens;
	}


	public boolean possuiVoucher() {
		return voucher;
	}
	

}
