package org.athoscastro.whatsapp;

public class Whatsapp implements Share {
    @Override
    public void to(String number) {
        System.out.println("Sending message to number " + number);
    }

    @Override
    public void caption(String description) {
        System.out.println("Image caption: " + description);
    }

    @Override
    public void picture(String picture) {
        System.out.println("URL of image: " + picture);
    }

    @Override
    public void send() {
        System.out.println("Message sent.");
    }
}
