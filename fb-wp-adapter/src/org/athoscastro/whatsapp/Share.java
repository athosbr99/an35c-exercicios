package org.athoscastro.whatsapp;

public interface Share {
    void to(String number);
    void caption(String description);
    void picture(String picture);
    void send();
}
