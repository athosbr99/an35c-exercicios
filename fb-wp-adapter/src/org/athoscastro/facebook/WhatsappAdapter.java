package org.athoscastro.facebook;

import org.athoscastro.whatsapp.Whatsapp;

public class WhatsappAdapter extends Facebook {
    private Whatsapp wa;

    public WhatsappAdapter(Whatsapp wa) {
        this.wa = wa;
    }

    @Override
    public void para(String amigo) {
        this.wa.to(amigo);
    }

    @Override
    public void foto(String foto) {
        this.wa.picture(foto);
    }

    @Override
    public void texto(String descricao) {
        this.wa.caption(descricao);
    }

    @Override
    public void enviar() {
        this.wa.send();
    }
}
