package org.athoscastro.facebook;

public interface Compartilha {
    void tipo(String tipo);
    void para(String amigo);
    void foto(String foto);
    void texto(String descricao);
    void enviar();
}
