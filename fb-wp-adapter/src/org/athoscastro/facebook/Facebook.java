package org.athoscastro.facebook;

public class Facebook implements Compartilha {
    @Override
    public void tipo(String tipo) {
        System.out.printf("Tipo de compartilhamento: " + tipo);
    }

    @Override
    public void para(String amigo) {
        System.out.println("Amigo destinatário: " + amigo);
    }

    @Override
    public void foto(String foto) {
        System.out.println("URL da foto: " + foto);
    }

    @Override
    public void texto(String descricao) {
        System.out.println("Descrição da foto: " + descricao);
    }

    @Override
    public void enviar() {
        System.out.println("Mensagem enviada.");
    }
}
