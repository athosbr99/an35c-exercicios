package org.athoscastro.interfaces;

public interface Observer {

    void update();
}
