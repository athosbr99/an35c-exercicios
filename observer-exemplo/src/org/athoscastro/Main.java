package org.athoscastro;

import org.athoscastro.classes.Mobile;
import org.athoscastro.classes.UsersService;

public class Main {

    public static void main(String[] args) {
        UsersService users = new UsersService();
        Mobile mobile = new Mobile(users);
        users.attach(mobile);
        users.setState("Jão");
        users.setState("Marquinho");
    }
}
