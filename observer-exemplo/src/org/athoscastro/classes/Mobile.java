package org.athoscastro.classes;

import org.athoscastro.interfaces.Observer;

public class Mobile implements Observer {

    protected UsersService usuarios;

    public Mobile(UsersService usuarios){
        this.usuarios = usuarios;
    }

    @Override
    public void update() {
        System.out.println("Chat pra celular - Usuários online:");
        for(String nome: this.usuarios.getState().lista) {
            System.out.println(nome);
        }
    }
}
