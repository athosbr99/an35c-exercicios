package org.athoscastro.classes;

import org.athoscastro.interfaces.Observer;
import org.athoscastro.interfaces.Subject;

import java.util.ArrayList;
import java.util.List;

public class UsersService implements Subject {
    protected List<Observer> observers;
    protected UsuariosConectados usuarios;

    public UsersService(){
        this.observers = new ArrayList<>();
        this.usuarios = new UsuariosConectados();
    }

    @Override
    public void attach(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public void dettach(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notificar() {
        for(Observer observer: observers) {
            observer.update();
        }
    }

    public void setState(String usuario) {
        this.usuarios.lista.add(usuario);
        notificar();
    }

    public UsuariosConectados getState() {
        return this.usuarios;
    }
}
