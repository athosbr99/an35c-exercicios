package org.athoscastro.app;

import org.athoscastro.builder.CartaoCreditoBradescoBuilder;
import org.athoscastro.builder.CartaoCreditoBuilder;
import org.athoscastro.cartao.CartaoCredito;

public class Main {

    public static void main(String[] args) {
        CartaoCreditoBuilder builder = new CartaoCreditoBradescoBuilder();
        CartaoCredito cartao = builder
                .emNomeDe("João Amoedo")
                .geraCodigoSeguranca()
                .geraSenha()
                .constroi();
        System.out.println(cartao.identifica());
    }
}
