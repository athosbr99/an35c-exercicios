package org.athoscastro.cartao;

public abstract class CartaoCredito {
    protected String nome;
    protected int senha;
    protected int codigoSeguranca;
    protected String bandeira;

    public abstract String identifica();

    public String getNome() {
        return nome;
    }

    public int getSenha() {
        return senha;
    }

    public int getCodigoSeguranca() {
        return codigoSeguranca;
    }

    public String getBandeira() {
        return bandeira;
    }
}
