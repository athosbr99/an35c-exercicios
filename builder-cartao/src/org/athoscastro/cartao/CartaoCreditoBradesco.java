package org.athoscastro.cartao;

public class CartaoCreditoBradesco extends CartaoCredito {

    public CartaoCreditoBradesco(String nome, int codigoSeguranca, int senha) {
        this.nome = nome;
        this.codigoSeguranca = codigoSeguranca;
        this.senha = senha;
    }

    public String identifica() {
        return "Este cartão de crédito é do Bradesco.";
    }
}
