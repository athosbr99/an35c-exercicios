package org.athoscastro.builder;

import org.athoscastro.cartao.CartaoCredito;
import org.athoscastro.cartao.CartaoCreditoBradesco;

import java.util.Random;

public class CartaoCreditoBradescoBuilder extends CartaoCreditoBuilder {
    @Override
    public CartaoCreditoBuilder emNomeDe(String nome) {
        this.nome = nome;
        return this;
    }

    @Override
    public CartaoCreditoBuilder geraSenha() {
        Random r = new Random();
        this.senha = r.nextInt(1000);
        return this;
    }

    @Override
    public CartaoCreditoBuilder geraCodigoSeguranca() {
        Random r = new Random();
        this.codigoSeguranca = r.nextInt(1000);
        return this;
    }

    @Override
    public CartaoCredito constroi() {
        return new CartaoCreditoBradesco(this.nome, this.codigoSeguranca, this.senha);
    }
}
