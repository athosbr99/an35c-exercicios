package org.athoscastro.builder;

import org.athoscastro.cartao.CartaoCredito;

public abstract class CartaoCreditoBuilder {

    protected String nome;
    protected int senha;
    protected int codigoSeguranca;
    protected String bandeira;

    public abstract CartaoCreditoBuilder emNomeDe(String nome);

    public abstract CartaoCreditoBuilder geraSenha();

    public abstract CartaoCreditoBuilder geraCodigoSeguranca();

    public abstract CartaoCredito constroi();

}
