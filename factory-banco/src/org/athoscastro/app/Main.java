package org.athoscastro.app;

import org.athoscastro.cartao.CartaoCredito;
import org.athoscastro.fornecedor.*;

public class Main {

    public static void main(String[] args) {
        CartaoCredito cartao;
        FornecedorCartao itau = new Itaucard();
        cartao = itau.criarCartao();
        System.out.println(cartao.getCodigo());
        System.out.println(cartao.getLimite());
        System.out.println(cartao.getSenha());
        System.out.println(cartao.identificar());
    }
}
