package org.athoscastro.fornecedor;

import org.athoscastro.cartao.CartaoCredito;

public abstract class FornecedorCartao {

    public abstract CartaoCredito criarCartao();

}
