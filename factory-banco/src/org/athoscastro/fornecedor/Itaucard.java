package org.athoscastro.fornecedor;

import org.athoscastro.cartao.CartaoCredito;
import org.athoscastro.cartao.CartaoCreditoItauVisa;

public class Itaucard extends FornecedorCartao {
    @Override
    public CartaoCredito criarCartao() {
        CartaoCredito cartao = new CartaoCreditoItauVisa();
        cartao.liberaLimite();
        cartao.codigoSeguranca();
        cartao.senha();
        cartao.emitir();
        cartao.enviar();
        return cartao;
    }
}
