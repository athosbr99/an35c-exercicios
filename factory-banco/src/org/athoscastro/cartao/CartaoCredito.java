package org.athoscastro.cartao;

public abstract class CartaoCredito {
    protected String senha;
    protected String codigo;
    protected Double limite;

    public abstract void liberaLimite();
    public abstract void codigoSeguranca();
    public abstract void senha();
    public abstract void emitir();
    public abstract void enviar();
    public abstract String identificar();

    public String getSenha() {
        return senha;
    }

    public String getCodigo() {
        return codigo;
    }

    public Double getLimite() {
        return limite;
    }
}
