package org.athoscastro.cartao;

import java.util.Random;

public class CartaoCreditoItauVisa extends CartaoCredito {

    @Override
    public void liberaLimite() {
        this.limite = 1.000;
    }

    @Override
    public void codigoSeguranca() {
        Random r = new Random();
        this.codigo = Integer.toString(r.nextInt(1000));
    }

    @Override
    public void senha() {
        Random r = new Random();
        this.senha = Integer.toString(r.nextInt(1000));
    }

    @Override
    public void emitir() {
        System.out.println("Cartão emitido.");
    }

    @Override
    public void enviar() {
        System.out.println("Cartão enviado.");
    }

    @Override
    public String identificar() {
        return "Este é um cartão de crédito Itaucard Visa.";
    }
}
